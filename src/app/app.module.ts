import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';


import {ApigooglePage} from '../pages/apigoogle/apigoogle'
import { HomePage } from '../pages/home/home';
import {PopupshowPage} from '../pages/popupshow/popupshow'
import {MoneyPage} from '../pages/money/money'
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GoogleCloudVisionServiceProvider } from '../providers/google-cloud-vision-service/google-cloud-vision-service';
import {MenuPage} from '../pages/menu/menu'
import {Camera} from '@ionic-native/camera';
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {AboutPage} from '../pages/about/about';


import {environment} from '../environment'
import {HttpModule} from '@angular/http'
import {AssignPage} from '../pages/assign/assign';
@NgModule({
  declarations: [
    MyApp,
    MoneyPage,
    ApigooglePage,
    HomePage,
    PopupshowPage,
    AboutPage,
    
    MenuPage,AssignPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp,{scrollPadding: false,
      scrollAssist: false,
      autoFocusAssist: false,}),
    AngularFireModule.initializeApp
    (environment.firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MoneyPage,
    ApigooglePage,PopupshowPage,
    HomePage,AboutPage,MenuPage,AssignPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    AngularFireModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,MoneyPage,
    
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GoogleCloudVisionServiceProvider
  ]
})
export class AppModule {}
import { Component } from '@angular/core';
import { IonicPage,  NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';

/**
 * Generated class for the PopupshowPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-popupshow',
  templateUrl: 'popupshow.html',
})
export class PopupshowPage {
  car_price:number ;
  moneydown:number;
  period:number;//งวด
  interest:number;//ดอกเบี้ย
  finance:number;
  interestyear:number;
  totalinterestyear:number;
  totalpayreal:number;
  period_month:number;
  
  constructor(public viewCtrl : ViewController ,public navParams:NavParams) {
    this.financecontrol(this.car_price,this.moneydown,this.period,this.interest)
    
  }    

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalPage'); console.log(this.car_price)
    this.car_price=this.navParams.get('car_price');;
    this.moneydown=this.navParams.get('moneydown');
    this.period=this.navParams.get('period');
    this.interest=this.navParams.get('interest');
    this.financecontrol(this.car_price,this.moneydown,this.period,this.interest)
  }
  public closeModal(){
    this.viewCtrl.dismiss();
  }
  public financecontrol(car_price,moneydown,period,interest){
    this.finance=this.car_price-this.moneydown;
    this.interestyear =this.finance*(this.interest/100);
    this.totalinterestyear= this.interestyear*(this.period/12);
    this.totalpayreal=this.totalinterestyear+this.finance;
    this.period_month=this.totalpayreal/this.period;
    console.log(this.finance);
    console.log(this.totalinterestyear);
    console.log(this.period_month);
    console.log(this.finance,this.totalinterestyear,this.period_month,this.totalpayreal)
    
    
  }
    
}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PopupshowPage } from './popupshow';

@NgModule({
  declarations: [
    PopupshowPage,
  ],
  imports: [
    IonicPageModule.forChild(PopupshowPage),
  ],
})
export class PopupshowPageModule {}

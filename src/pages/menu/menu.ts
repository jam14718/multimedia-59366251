import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {AssignPage} from '../assign/assign';
import {MoneyPage} from '../money/money';
import {ApigooglePage} from '../apigoogle/apigoogle';

@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {
  

  constructor(public navCtrl: NavController) {
   
  }
 money(){
    this.navCtrl.push(MoneyPage);
    console.log("moneyPage")
    }
  google(){
      this.navCtrl.push(ApigooglePage);
      console.log("opengoogleapi")
    }
  Assignment(){
      this.navCtrl.push(AssignPage);
      console.log("TabsPage")
    }
     
}

import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {AboutPage} from '../about/about';
import  {HomePage} from '../home/home';


/**
 * Generated class for the AssignPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-assign',
  templateUrl: 'assign.html',
})
export class AssignPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AssignPage');
  }
 home(){
    this.navCtrl.push(HomePage);
    console.log("home")
    }
    about(){
      this.navCtrl.push(AboutPage);
      console.log("about")
      }
     
}

import { Component } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';
import { ModalController } from 'ionic-angular';
import { PopupshowPage } from '../popupshow/popupshow';
import { NavController } from 'ionic-angular';

/**
 * Generated class for the MoneyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-money',
  templateUrl: 'money.html',
})
export class MoneyPage {
  public car_price:string;
  public moneydown;
  public period;
  public interest;
  public year;
  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController) {
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MoneyPage');
  }
  public openModal(){
    
    
    this.navCtrl.push(PopupshowPage , 
      {car_price:this.car_price,
        moneydown:this.moneydown,
        period:this.period,
        interest:this.interest,
        year:this.year}
    );
    
    console.log(this.car_price);
    console.log(this.moneydown);
    console.log(this.period);
    console.log(this.interest);
    console.log(this.year);

}
  

}
